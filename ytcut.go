package main

import (
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rylio/ytdl"
)

// 1e exchanged via WS to communicate the status
type Message struct {
	YoutubeURL string `json:"YoutubeURL"`
	Status     string `json:"Status"`
	StartTime  string `json:"StartTime"`
	EndTime    string `json:"EndTime"`
}

func convertToMP3(startTime string, endTime string, artifactFileName string, convertedFileName string) {
	// look for ffmpeg in path
	ffmpeg, err := exec.LookPath("ffmpeg")
	if err != nil {
		log.Println("ffmpeg not found")
	} else {
		log.Println("Extracting audio ..")

		// start + endtime
		cmd := exec.Command(ffmpeg, "-y", "-loglevel", "quiet", "-i", artifactFileName, "-ss", startTime, "-to", endTime, "-vn", convertedFileName)

		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			log.Println("Failed to extract audio:", err)
		} else {
			log.Println()
			log.Println("Extracted audio:", convertedFileName)
		}
	}
}

// refactor download function to getMeta, which returns video object
// create pure download function which takes video object
// use the video object to generate good filename

func getVideoMeta(youtubeID string) *ytdl.VideoInfo {
	// get the video object from youtube (with metdata)
	videoinfo, err := ytdl.GetVideoInfo("https://youtube.com/watch?v=" + youtubeID)
	if err != nil {
		log.Fatal(err)
	}

	return videoinfo
}

func downloadFromYoutube(youtubeID string, video *ytdl.VideoInfo) {
	// download the video and write to file
	file, _ := os.Create(youtubeID + ".mp4")
	video.Download(video.Formats[0], file)
}

// RequestHandler - parses the request. TODO: validates.
// Starts the download if the file is not on disk yet, and serves it
func requestHandler(w http.ResponseWriter, r *http.Request) {
	var url = r.URL.Query().Get("url")
	var startTime = r.URL.Query().Get("start")
	var endTime = r.URL.Query().Get("end")
	var splitQuery = strings.SplitAfter(url, "v=")
	var ytID string

	if len(splitQuery) > 1 { // expected case
		ytID = strings.SplitAfter(url, "v=")[1] // FIXME: only works for full youtube links, not ids
	} else { // invalid query. return 400
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// generate some strings
	var convertedFileName = ytID + ".mp3"
	var artifactFileName = ytID + ".mp4"

	log.Println("url:", url, " start:", startTime, " end:", endTime, " ytID: ", ytID, "File:", artifactFileName)

	// get metadata
	videoinfo := getVideoMeta(ytID)

	var filenameHeader = "attachment; filename=" + videoinfo.Title + ".mp3" // triggers download

	downloadFromYoutube(ytID, videoinfo)

	convertToMP3(startTime, endTime, artifactFileName, convertedFileName)

	// once the converted file exists, we serve it
	w.Header().Set("Content-Disposition", filenameHeader)
	http.ServeFile(w, r, convertedFileName)

	// once we're done, remove the files from disk
	os.Remove(artifactFileName)
	os.Remove(convertedFileName)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/yt", requestHandler)

	log.Fatal(http.ListenAndServe(":1337", r))
}
