### API for fetching youtube videos as MP3s.
depends on my fork: https://github.com/NilsKaden/go-get-youtube

with (todo) start and endtime cut via ffmpeg (possibly)

this somehow doesnt seem to work for older youtube videos (> 5 years).
possible due to different encoding (avi?)


try using the following library/container for downloading from youtube. Maybe it supports ffmpeg better?
https://hub.docker.com/r/vimagick/youtube-dl

## query params:
url (string): full youtube url#
start (float): startime in seconds#
end (float): endtime in seconds#

# example call: http://nilsk.dynu.net:1337/yt?url=https://www.youtube.com/watch?v=mTKky7DXftE

## Docker
run the docker container with 
`docker-compose up`

or add the -d flag to 
`docker-compose up -d`

you might need to build the image before running the up-command. Use: 
`docker-compose build`