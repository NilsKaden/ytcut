# Start from golang v1.11 base image
FROM golang:latest as builder

# Add Maintainer Info
LABEL maintainer="Nils Kaden <nils@nilsk.dev>"

WORKDIR src/app

# Copy everything from the current directory to the PWD(Present Working Directory) inside the container
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./bin/ytcut ./

######## Start a new stage from scratch #######
FROM alpine:latest  

RUN apk --no-cache add ca-certificates
RUN apk add --no-cache ffmpeg

WORKDIR /root/

# Copy the Pre-built binary file from the previous stage
COPY --from=builder go/src/app/bin/ytcut .

EXPOSE 1337

CMD ["./ytcut"]
